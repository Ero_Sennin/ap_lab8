void parallelMultMV (void *args);

struct Vector
{
	int rows;
	int *v;
};

struct mv
{
	struct Matrix A;
	struct Vector v;
	struct Vector* w;
	int st;
	int inc;
};
