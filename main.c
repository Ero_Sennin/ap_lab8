#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include "matrix.h"
#include "vector.h"

#define RANGE 101
#define MAX_THREADS 8
#define DIMENSION 16

int main (void)
{
	srand (time(NULL));

	char opt = '1';
		
	printf("Press 1 for matrix matrix multiplication and press 2 for matrix vector multiplication\n");

	opt = getchar();

	if (opt == '1')
	{
		struct Matrix A;
		struct Matrix B;
		struct Matrix result;

		A.rows = DIMENSION; A.columns=DIMENSION; A.m = malloc(sizeof(int)*A.rows*A.columns);
		B.rows = DIMENSION; B.columns=DIMENSION; B.m = malloc(sizeof(int)*B.rows*B.columns);
		result.rows = DIMENSION; result.columns=DIMENSION; result.m = malloc(sizeof(int)*result.rows*result.columns);

		int i=0;

		for (i=0; i<A.rows*A.columns; ++i)
			A.m[i] = (((double)rand()/(double)RAND_MAX) * RANGE);

		for (i=0; i<B.rows*B.columns; ++i)
			B.m[i] = (((double)rand()/(double)RAND_MAX) * RANGE);

		for(i=0; i<result.rows*result.columns; i++)
			result.m[i]=0;

		struct mm args[MAX_THREADS];
		pthread_t thread[MAX_THREADS];
		int rc;

		for (i=0; i<MAX_THREADS; i++)
		{
			args[i].A=A;
			args[i].B=B;
			args[i].result=&result;
			args[i].st=i;
			args[i].inc=MAX_THREADS;
		}
	
		for (i=0; i<MAX_THREADS; i++)
		{
			rc = pthread_create(&thread[i], NULL, parallelMultMM, (void *)&args[i]); 
			if (rc)
			{
				printf("ERROR; return code from pthread_is %d\n", rc);
				exit(-1);
			}
		}

		for (i=0; i<MAX_THREADS; i++)
			pthread_join(thread[i], NULL);

		
		printf("done\n");
	}

	else if (opt == '2')
	{
		struct Matrix A;
		struct Vector v;
		struct Vector w;
	
		A.rows = DIMENSION; A.columns=DIMENSION; A.m = malloc(sizeof(int)*A.rows*A.columns);
		v.rows = DIMENSION; v.v = malloc(sizeof(int)*v.rows);
		w.rows = DIMENSION; w.v = malloc(sizeof(int)*w.rows);

		int i=0;

		for (i=0; i<A.rows*A.columns; ++i)
			A.m[i] = (((double)rand()/(double)RAND_MAX) * RANGE);

		for (i=0; i<v.rows; ++i)
			v.v[i] = (((double)rand()/(double)RAND_MAX) * RANGE);

		for(i=0; i<w.rows; i++)
			w.v[i]=0;

		struct mv args[MAX_THREADS];
		pthread_t thread[MAX_THREADS];
		int rc;

		for (i=0; i<MAX_THREADS; i++)
		{
			args[i].A=A;
			args[i].v=v;
			args[i].w=&w;
			args[i].st=i;
			args[i].inc=MAX_THREADS;
		}

		for (i=0; i<MAX_THREADS; i++)
		{
			rc = pthread_create(&thread[i], NULL, parallelMultMV, (void *)&args[i]); 
			if (rc)
			{
				printf("ERROR; return code from pthread_is %d\n", rc);
				exit(-1);
			}
		}

		for (i=0; i<MAX_THREADS; i++)
			pthread_join(thread[i], NULL);

		printf("done\n");
	}

	else
		printf("Invalid choice\n");

	return 0;
}
