#include "matrix.h"

void parallelMultMM (void *args)
{
	int i,j,k;
	struct mm* data = (struct data*)args;

	if (data->A.columns != data->B.rows)
	{
		printf("Error. Columns of first matrix are not equal to rows of the second matrix, cannot multiply.\n");
		getchar();
		exit(1);
	}

	for ( i=data->st; i<data->A.rows; i+=data->inc)
		for( j=0; j<data->B.columns; ++j)
			for( k=0; k<data->A.columns; ++k)
				data->result->m[i*data->result->columns + j] += data->A.m[i*data->A.columns+k] * data->B.m[k*data->B.columns+j];

	//pthread_exit(NULL);
}
