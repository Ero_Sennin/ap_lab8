void parallelMultMM (void *args);

struct Matrix
{
	int rows;
	int columns;
	int *m;
};

struct mm
{
	struct Matrix A;
	struct Matrix B;
	struct Matrix* result;
	int st;
	int inc;
};
