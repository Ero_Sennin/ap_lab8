OBJS = main.o vector.o matrix.o 

matrix.out : $(OBJS)
	gcc -o matrix.out $(OBJS) -pthread -w

main.o : main.c
	gcc -c main.c -pthread -w

vector.o : vector.c
	gcc -c vector.c -pthread -w

matrix.o : matrix.c
	gcc -c matrix.c -pthread -w

clean :
	rm $(OBJS) matrix.out
