#include "matrix.h"
#include "vector.h"

void parallelMultMV (void *args)
{
	int i,j;
	struct mv* data = (struct data*)args;

	if (data->A.columns != data->v.rows)
	{
		printf("Error. Number of columns in the matrix is not equal to number of rows in the vector, cannot multiply.");
		exit(1);
	}

	for( i=data->st; i<data->A.rows; i+=data->inc)
		for( j=0; j<data->A.columns; ++j)
			data->w->v[i] += data->A.m[i*data->A.columns+j] * data->v.v[j];			

	//pthread_exit(NULL);
}
